# OpenMF

We aim to create free and open-source tools for the game [Mafia](https://en.wikipedia.org/wiki/Mafia_(video_game)). The ultimate goal is to recreate the original game engine from scratch, allowing for enhancements and compatibility with modern platforms. We take inspiration in [OpenMW](https://openmw.org/en/).

We respect copyrights and intelectual property and you need to own the original game with all the game data in order for our tools to be useful to you.

The project is in very early stage and at the moment can offer libraries for loading some of the game files and a model/world viewer with already a pretty nice rendering. If you want to help, feel free to join us via a pull-request, issue tracker or other means.

![](https://github.com/OpenMafia/openmf/blob/master/resources/screens.jpg?raw=true)
